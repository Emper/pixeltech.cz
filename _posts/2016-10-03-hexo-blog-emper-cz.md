---
layout: post
title: Hexo blog Emper.cz
description: Generátor statických stránek Hexo není k&nbsp;zahození.
tags: [hexo, static]
image: default-hexo.png
---

Do rodiny přibývá nový blogísek. Tentokrát nikoli Jekyll přírůstek, nýbrž [Hexo](http://hexo.io).

Při vytváření správné šablony jsem si neustále říkal, zda by to nebylo jednoduší se starým dobrým Jekyllem. Ale nakonec se vše povedlo tak, jak to bylo naplánováno.

[Osobní web o webech, Emper](http://emper.cz), kde budou vycházet články, návody, prezentace, učební materiály a další zbytečnosti o web designu, web vývoji a&nbsp;tak.
