---
layout: post
title: Projekt Kuchtit
description: Něco jako studentská web kuchařka. Aplikace vyhledává recepty podle toho, které suroviny máte k dispozici.
tags: [laravel]
image: kuchtit.jpg
published: false
---

První větší projekt je (bude) webová aplikace na recepty. Pracovní název Kuchtit.cz, ale ten se ještě může změnit.

Hlavní idea spočívá v tom, že uživatel navštíví stránku a hned jako první uvidí obrovské textové pole, do kterého začne vypisovat suroviny, ze kterých chce něco uvařit. Tedy ty suroviny, které má asi někde doma. Aplikace pak uživateli nabídne recepty, na které má dostatek surovin, popřípadě recepty, na které chybí menší počet surovin.

Dostupné recepty by měly být spíše jednoduchá, rychlá a ne moc drahá jídla. Něco jako studentská web kuchařka.

První fáze je projekt zprovoznit na tuhle výše popsanou úroveň. Tu už máme za sebou.

Jako další přichází rozšiřování aplikace a různé funkce. Na plánu jich je hned několik:

- hodnocení receptů
- registrace a přihlašování uživatelů
- ukládání oblíbených receptů a surovin uživatele
- vkládání vlastních receptů uživatelem
- cenové kategorie surovin
- podrobné vyhledávání receptů
- doporučené recepty na základě oblíbených, často používaných a dobře hodnocených
