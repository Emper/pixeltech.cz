---
layout: post
title: Nálož šablon a vqmod úprav
description: Na začátek několik šablon a mikropluginů pro e-commerce platformu OpenCart.
tags: [opencart]
image: default-opencart.png
---

No jo, prostě OpenCart. Najde se hromada odpůrců s názory, že je to hrozný systém, špatně naprogramovaný, plný chyb, že se špatně modifikuje a že by se vlastně neměl ani používat. Tak jednostranné to ale není, protože má stále má co nabídnout a pro mnohé případy bez problému postačí. V roce 2015 byl tento open source e-commerce software údajně zodpovědný za zhruba 6% internetových obchodů, čímž porazil OSCommerce, ZenCart i Shopify.

OpenCart nebyl první e-commerce systém, se kterým jsem setkal. Pokud nebudu počítat menší administrační rozhraní pro e-shopy tvořené konkrétní firmou vždy tak nějak na míru zákazníkovi, první větší a volně dostupný pro mne byl PrestaShop. Ten může OpenCart v mnohém úplně rozdrtit, ale tohle není článek o porovnávání PrestaShopu s OpenCartem.

Důležité je, že OpenCart je jednoduchý a rychle se do něj dostanete. Podporuje vícejazyčné e-shopy a více měn. Je rychlý a neklade velké nároky na server. MVC architektura aplikace a velmi přívětivé administrační rozhraní nejsou na škodu. Co se týká práce na úrovni správy e-shopu (vkládat a upravovat produkty, vyřizovat objednávky a faktury...), tak naučit se tyto dovednosti je na pár minut. Pro programátora, který chce systému porozumět hlouběji a upravit dle požadavků klienta, to taky není vůbec nic těžkého. Postupem času se ale jak programátor, tak hlavně správce webu, budou setkávat s problémy a zbytečnostmi, které nejsou úplně fajn.

Pro ušetření času je tu proto hned několik [vqmod úprav]({{ site.url }}/pluginy/), a to zejména administračního rozhraní, aby se s ním dalo pracovat pohodlněji.

A taky [pár šablon]({{ site.url }}/sablony/) vycházejících z původní default šablony OpenCartu, a to ve smyslu, že zachovávají konvecne původní šablony, názvy tříd a id, ale jsou obohaceny o vlastní vzhled.
