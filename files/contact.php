---
permalink: /php/contact.php
---
{% raw %}<?php

// if(isset($_POST['contact-submit'])){

  $mail = $_POST['mail'];
  $name = $_POST['name'];
  $comment = $_POST['comment'];
  $error = false;

  // echo $mail." | ".$name." | ".$comment;

  if (strlen($name) > 4 && strlen($mail) > 8 && strlen($comment) > 16 &&
      filter_var($mail, FILTER_VALIDATE_EMAIL) &&
      $_POST['h1'] == '' && $_POST['h2'] == '' && $_POST['h3'] == 'x_5g9avl'
    ) {

    $subject = 'PXT: '.$name;
    $to = 'info@pixeltech.cz';
    $message =  "<b>$name</b> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; $mail<br><br>$comment";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
    $headers .= "From: " . $mail . "\r\n" .
    "Reply-To: " . $mail . "\r\n" .
    "X-Mailer: PHP/" . phpversion();

    mail($to,$subject,$message,$headers);
    echo "OK";

  }

// }

?>{% endraw %}
